import 'dart:convert';

import 'package:demo_app/models/register_model.dart';
import 'package:demo_app/models/register_response_model.dart';
import 'package:demo_app/utils/cons.dart';
import 'package:demo_app/utils/helpers.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class RegisterController with ChangeNotifier{
  bool _isUsernameValid=true;
  bool _isEmailValid=true;
  bool _isPassValid=true;

  String _usernameErrorMsg='';
  String _emailErrorMsg='';
  String _passErrorMsg='';

  bool isLoading=false;


  String get usernameErrorMsg => _usernameErrorMsg;

  set usernameErrorMsg(String value) {
    _usernameErrorMsg = value;
  }

  bool get isEmailValid => _isEmailValid;

  set isEmailValid(bool value) {
    _isEmailValid = value;
  }

  bool get isUsernameValid => _isUsernameValid;

  set isUsernameValid(bool value) {
    _isUsernameValid = value;
  }


  bool get isPassValid => _isPassValid;

  set isPassValid(bool value) {
    _isPassValid = value;
  }

  bool formValidator(String username,String email,String password){
    //bool isValid=true;
    if(username.isEmpty)
    {
      _isUsernameValid=false;
      _usernameErrorMsg='Enter your username';
    }
    else{
      _isUsernameValid=true;
    }
    if(email.isEmpty)
      {
        _emailErrorMsg='Enter your email';
        _isEmailValid=false;
      }
    else if(!isEmailValidFormat(email)){
      _emailErrorMsg='Invalid email format';
      _isEmailValid=false;
    }
    else{
      _isEmailValid=true;
    }
    if(password.isEmpty)
    {
      _passErrorMsg='Enter your password';
      _isPassValid=false;
    }
    else if(password.length<6)
      {
        _passErrorMsg='Password at least 6 characters';
        _isPassValid=false;
      }
    else{
      _isPassValid=true;

    }

    notifyListeners();

    return _isPassValid && _isEmailValid && _isUsernameValid;
  }

  String get emailErrorMsg => _emailErrorMsg;

  set emailErrorMsg(String value) {
    _emailErrorMsg = value;
  }

  String get passErrorMsg => _passErrorMsg;

  set passErrorMsg(String value) {
    _passErrorMsg = value;
  }


  Future<bool> userRegister(RegisterModel model) async {
    late RegisterResponseModel responseModel;
    isLoading=true;
    notifyListeners();
    var url = Uri.parse(baseUrl + 'wp-json/wp/v2/users/register');
    Map<String, String> headers = {
      "Content-Type": "application/json"
    };
    //print(model.username!+'='+model.email!+'='+model.password!);
    var response = await http.post(url, body: json.encode(model),headers: headers);

    print(response.body);

    isLoading=false;
    notifyListeners();
    if(response.statusCode==200)
    {
      responseModel=RegisterResponseModel.fromJson(json.decode(response.body));
      return true;
    }
    else if(response.statusCode==400)
    {
      responseModel=RegisterResponseModel.fromJson(json.decode(response.body));
      toaster(responseModel.message,toastType: GeneralConstant.FAILED);
      return false;
    }
    else{
      toaster(serverError,toastType: GeneralConstant.FAILED);
      return false;
    }

  }
}