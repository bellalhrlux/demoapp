/// code : 200
/// message : "User 'dev11' Registration was Successful"


class RegisterResponseModel {
  RegisterResponseModel({
      int? code, 
      String? message,}){
    _code = code;
    _message = message;
}

  RegisterResponseModel.fromJson(dynamic json) {
    _code = json['code'];
    _message = json['message'];
  }
  int? _code;
  String? _message;

  int? get code => _code;
  String? get message => _message;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['code'] = _code;
    map['message'] = _message;
    return map;
  }

}