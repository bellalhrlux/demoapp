import 'package:demo_app/models/login_response_model.dart';
import 'package:demo_app/utils/app_colors.dart';
import 'package:demo_app/utils/routes.dart';
import 'package:demo_app/views/home.dart';
import 'package:demo_app/views/login.dart';
import 'package:demo_app/views/register.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: appThemeColor,
      ),
      home: const Login(),
      routes: <String, WidgetBuilder>{
        registerRoute: (BuildContext context) =>const Register(),
        homeRoute: (BuildContext context) => Home(),
      }
    );
  }
}

