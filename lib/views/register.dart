import 'package:demo_app/constrollers/register_controller.dart';
import 'package:demo_app/models/register_model.dart';
import 'package:demo_app/utils/common_widgets.dart';
import 'package:demo_app/utils/cons.dart';
import 'package:demo_app/utils/dimens.dart';
import 'package:demo_app/utils/routes.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class Register extends StatefulWidget {
  const Register({Key? key}) : super(key: key);

  @override
  _RegisterState createState() => _RegisterState();
}

class _RegisterState extends State<Register> {
  /// declare home controller
  late RegisterController _registerController;

  ///Declare controller for textField
  final _usernameController = TextEditingController();
  final _emailController = TextEditingController();
  final _passwordController = TextEditingController();

  Text _registerTitle() => const Text(
        registerNow,
        style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold),
      );

  Padding _registerSubtitle() => const Padding(
        padding: EdgeInsets.only(top: 15, bottom: 15),
        child: Text(
          loginDetailMsg,
          style: TextStyle(fontSize: 12, fontWeight: FontWeight.w300),
        ),
      );

  Padding _registerForm() => Padding(
        child: Column(
          children: [
            textFieldContainer('Enter username',
                controller: _usernameController),
            if (!_registerController.isUsernameValid)
              textFieldErrorMsg(msg: _registerController.usernameErrorMsg),
            const SizedBox(
              height: 20,
            ),
            textFieldContainer('Enter email', controller: _emailController),
            if (!_registerController.isEmailValid)
              textFieldErrorMsg(msg: _registerController.emailErrorMsg),
            const SizedBox(
              height: 20,
            ),
            textFieldContainer('Enter Password',
                obscureValue: true, controller: _passwordController),
            if (!_registerController.isPassValid)
              textFieldErrorMsg(msg: _registerController.passErrorMsg)
          ],
        ),
        padding: const EdgeInsets.only(left: 20, right: 20),
      );

  Padding _backToLogin() => Padding(
        padding: const EdgeInsets.only(left: 20, right: 20, top: 30),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            const Text('Already have an account?'),
            InkWell(
              child: const Text(
                'Login',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
              onTap: () {
                Navigator.pushReplacementNamed(context, loginRoute);
              },
            ),
          ],
        ),
      );

  Padding _registerBtn() => Padding(
        padding: const EdgeInsets.only(left: 20, right: 20),
        child: SizedBox(
          height: buttonHeight,
          width: double.infinity,
          child: ElevatedButton(
              style: ElevatedButton.styleFrom(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(8))),
              onPressed: () async {
                var isValid = _registerController.formValidator(
                    _usernameController.text,
                    _emailController.text,
                    _passwordController.text);
                if (isValid) {
                  if (!_registerController.isLoading) {
                    var model=RegisterModel(
                        email: _emailController.text,
                        username: _usernameController.text,
                        password: _passwordController.text
                    );
                    var result = await _registerController.userRegister(model);
                    if (result) {
                      Navigator.pushReplacementNamed(context, loginRoute);
                    }
                    //Navigator.pushNamed(context, registerRoute);

                    //Navigator.pushNamedAndRemoveUntil(context, registerRoute, (route) => false);
                  }
                }
              },
              child: _registerController.isLoading
                  ? const CircularProgressIndicator(
                      color: Colors.white,
                    )
                  : const Text(
                      'Register',
                      style:
                          TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                    )),
        ),
      );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: null,
      backgroundColor: Colors.white,
      body: Center(
        child: SingleChildScrollView(
          child: ChangeNotifierProvider(
            create: (context) => RegisterController(),
            child: Consumer<RegisterController>(
              builder: (context, controller, child) {
                _registerController = controller;
                return Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    _registerTitle(),
                    _registerSubtitle(),
                    _registerForm(),
                    const SizedBox(
                      height: 40,
                    ),
                    _registerBtn(),
                    _backToLogin()
                  ],
                );
              },
            ),
          ),
        ),
      ),
    );
  }
}
