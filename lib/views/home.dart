import 'package:demo_app/constrollers/home_controller.dart';
import 'package:demo_app/models/login_response_model.dart';
import 'package:demo_app/models/user_profile_model.dart';
import 'package:demo_app/utils/app_colors.dart';
import 'package:demo_app/utils/common_widgets.dart';
import 'package:demo_app/utils/dimens.dart';
import 'package:demo_app/utils/helpers.dart';
import 'package:demo_app/utils/routes.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class Home extends StatefulWidget {
  Home({Key? key}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}



class _HomeState extends State<Home> {
  /// declare home controller
  late HomeController _homeController;
  ///Declare controller for textField
  final _firstNameController= TextEditingController();
  final _lastNameController= TextEditingController();
  final _emailController= TextEditingController();
  late UserProfileModel _profileModel;
  final _levelStyle=const TextStyle(fontSize: 15,fontWeight: FontWeight.bold);
  bool isLoad=false;
  Container _userAvater(UserProfileModel? result) =>Container(
    height: 80,
    width: 80,
    decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(50),
        border: Border.all(color: appThemeColor,width: 1),
        image: DecorationImage(
            image: NetworkImage(result?.avatarUrls?.image_96??'')
        )
    ),

  );

  _updateUi(HomeController controller)async{
    _homeController=controller;
     await _homeController.fetchUserInfo();
    _firstNameController.text=_homeController.userProfileModel.firstName??'';
    _lastNameController.text=_homeController.userProfileModel.lastName??'';
    _emailController.text=_homeController.userProfileModel.email??'';
  }

  Padding _profileInfo()=>Padding(padding: const EdgeInsets.only(top: 20),child: Column(
    mainAxisAlignment: MainAxisAlignment.start,
    crossAxisAlignment: CrossAxisAlignment.start,
    children: [
      Text('First Name : ',style: _levelStyle,),
      const SizedBox(height: 8,),
      textFieldContainer('Enter first name',controller: _firstNameController),
      const SizedBox(height: 15,),
      Text('Last Name : ',style: _levelStyle,),
      const SizedBox(height: 8,),
      textFieldContainer('Enter last name',controller: _lastNameController),
      const SizedBox(height: 15,),
      Text('Email : ',style: _levelStyle,),
      const SizedBox(height: 8,),
      textFieldContainer('Enter email',controller: _emailController,isEnable: false),
    ],
  ),);


  Padding _updateBtn()=>Padding(padding: const EdgeInsets.only(left: 0,right: 0,top: 30),child: SizedBox(
    height: buttonHeight,
    width: double.infinity,
    child: ElevatedButton(
        style: ElevatedButton.styleFrom(shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(8)
        )),
        onPressed: ()async{
           setState(() {
             isLoad=true;
           });
            var result=await _homeController.updateProfile(_homeController.userProfileModel.id.toString(), _firstNameController.text, _lastNameController.text);
            if(result)
              {
                toaster('Profile updated Successfully',toastType: GeneralConstant.SUCCESS);
              }
            setState(() {
              isLoad=false;
            });
        }, child: isLoad?const CircularProgressIndicator(color: Colors.white,):const Text('Update',style: TextStyle(fontSize: 16,fontWeight: FontWeight.bold),)),
  ),);


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: const Text('User Dashboard'),
        actions: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: InkWell(child: const Icon(Icons.logout,),onTap: (){
              Navigator.pushReplacementNamed(context,loginRoute);
            },),
          )
        ],
      ),
      body: Center(
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.only(left: 20,right: 20),
            child: ChangeNotifierProvider(
              create: (context)=>HomeController(),
              child: Consumer<HomeController>(
                builder: (context,controller,child){
                  if(!controller.init)
                    {
                      _updateUi(controller);
                      controller.init=true;
                    }
                  return Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      if(!_homeController.isLoading)
                      _userAvater(_homeController.userProfileModel),
                       if(!_homeController.isLoading)
                      _profileInfo(),
                      if(!_homeController.isLoading)
                      _updateBtn(),
                      if(_homeController.isLoading)
                       const CircularProgressIndicator()
                    ],
                  );
                },
              ),
            ),
          ),
        ),
      ),
    );
  }
}

