import 'package:flutter/material.dart';

const int baseColor=0xFF333333;
const Color lightWhite=Color(0xFFF6F9FF);




int rColor = 51;//67;
int gColor = 51;//170;
int bColor = 51;//84;

Map<int, Color> colorCodes = {
  50: Color.fromRGBO(rColor, gColor, bColor, .1),
  100: Color.fromRGBO(rColor, gColor, bColor, .2),
  200: Color.fromRGBO(rColor, gColor, bColor, .3),
  300: Color.fromRGBO(rColor, gColor, bColor, .4),
  400: Color.fromRGBO(rColor, gColor, bColor, .5),
  500: Color.fromRGBO(rColor, gColor, bColor, .6),
  600: Color.fromRGBO(rColor, gColor, bColor, .7),
  700: Color.fromRGBO(rColor, gColor, bColor, .8),
  800: Color.fromRGBO(rColor, gColor, bColor, .9),
  900: Color.fromRGBO(rColor, gColor, bColor, 1),
};
//Fair Mart
//MaterialColor appThemeColor = new MaterialColor(baseColor, colorCodes);
//Kinakata
MaterialColor appThemeColor = MaterialColor(baseColor, colorCodes);