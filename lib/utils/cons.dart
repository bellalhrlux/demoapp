import 'package:demo_app/models/login_response_model.dart';

const loginDetailMsg='Please enter the details below to continue';
const loginNow='Login Now';
const registerNow='Register Now';
const serverError='Something went wrong! please try again';
late LoginResponseModel globalLoginResModel;

/// Base url
const baseUrl='https://apptest.dokandemo.com/';