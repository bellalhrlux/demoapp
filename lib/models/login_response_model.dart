/// token : "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczpcL1wvYXBwdGVzdC5kb2thbmRlbW8uY29tIiwiaWF0IjoxNjM2MzUxNjE4LCJuYmYiOjE2MzYzNTE2MTgsImV4cCI6MTYzNjk1NjQxOCwiZGF0YSI6eyJ1c2VyIjp7ImlkIjoiMTAzIn19fQ.IJ7pnZIrRfE2kZmpM3Zo6GIGBgtcquJXQc9ZLW9r9Pg"
/// user_email : "test123@gmail.com"
/// user_nicename : "test123"
/// user_display_name : "test123"

class LoginResponseModel {
  LoginResponseModel({
    String? token,
    String? userEmail,
    String? userNicename,
    String? userDisplayName,
    String? code,
    String? message,
    Data? data,
  }) {
    _token = token;
    _userEmail = userEmail;
    _userNicename = userNicename;
    _userDisplayName = userDisplayName;
    _code = code;
    _message = message;
    _data = data;
  }

  String? get code => _code;

  LoginResponseModel.fromJson(dynamic json) {
    _token = json['token'];
    _userEmail = json['user_email'];
    _userNicename = json['user_nicename'];
    _userDisplayName = json['user_display_name'];
    _code = json['code'];
    _message = json['message'];
    _data = json['data'] != null ? Data.fromJson(json['data']) : null;
  }

  String? _token;
  String? _userEmail;
  String? _userNicename;
  String? _userDisplayName;

  String? _code;
  String? _message;
  Data? _data;

  String? get token => _token;

  String? get userEmail => _userEmail;

  String? get userNicename => _userNicename;

  String? get userDisplayName => _userDisplayName;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['token'] = _token;
    map['user_email'] = _userEmail;
    map['user_nicename'] = _userNicename;
    map['user_display_name'] = _userDisplayName;
    map['code'] = _code;
    map['message'] = _message;
    if (_data != null) {
      map['data'] = _data?.toJson();
    }
    return map;
  }

  String? get message => _message;

  Data? get data => _data;
}

class Data {
  Data({
    int? status,
  }) {
    _status = status;
  }

  Data.fromJson(dynamic json) {
    _status = json['status'];
  }

  int? _status;

  int? get status => _status;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['status'] = _status;
    return map;
  }
}
