import 'dart:convert';

import 'package:demo_app/models/user_profile_model.dart';
import 'package:demo_app/utils/cons.dart';
import 'package:demo_app/utils/helpers.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class HomeController with ChangeNotifier {
  bool isLoading = false;
  bool init = false;
  late UserProfileModel userProfileModel=UserProfileModel();

  Future<UserProfileModel> fetchUserInfo() async {
    isLoading = true;
    notifyListeners();


    Map<String, String> headers = {
      "Authorization": 'Bearer ' + globalLoginResModel.token.toString(),
      "Content-Type": "application/json"
    };


    var url = Uri.parse(baseUrl + 'wp-json/wp/v2/users/me');
    var response = await http.post(url, headers: headers);

    print(response.body);

    isLoading = false;
    notifyListeners();

    if (response.statusCode == 200) {
      userProfileModel = UserProfileModel.fromJson(json.decode(response.body));
      print('success=>'+response.body);
      return userProfileModel;
    } else {
      print('failed=>'+response.body);
      toaster(serverError, toastType: GeneralConstant.FAILED);
      return UserProfileModel();
    }
  }



  Future<bool> updateProfile(String userId,String fName,String lName) async {
    Map<String, String> headers = {
      "Authorization": 'Bearer ' + globalLoginResModel.token.toString(),
      "Content-Type": "application/json"
    };

    Map<String, String> bodyMap = {
      "first_name":fName,
      "last_name":lName
    };
    var url = Uri.parse(baseUrl + 'wp-json/wp/v2/users/$userId');

    var response = await http.post(url, body:json.encode(bodyMap),headers: headers);
    if (response.statusCode == 200) {
      //userProfileModel = UserProfileModel.fromJson(json.decode(response.body));

      return true;
    } else {
     // print('failed=>'+response.body);
      toaster(serverError, toastType: GeneralConstant.FAILED);
      return false;
    }
  }

}
