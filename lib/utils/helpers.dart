import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:html/parser.dart';
bool isEmailValidFormat(String email) {
  return RegExp(
      r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
      .hasMatch(email);
}

//here goes the function
String parseHtmlString(String htmlString) {
  final document = parse(htmlString);
  final String parsedString = parse(document.body!.text).documentElement!.text;
  return parsedString;
}

toaster(String? message,
    {GeneralConstant toastType = GeneralConstant.NONE,
      Toast duration = Toast.LENGTH_LONG}) {
  Color? bgColor, txtColor;

  //{GeneralConstant toastType=GeneralConstant.NONE,Color bgColor=Colors.white,Color txtColor=Colors.black
  switch (toastType) {
    case GeneralConstant.SUCCESS:
      bgColor = Colors.green;
      txtColor = Colors.white;
      break;
    case GeneralConstant.FAILED:
      bgColor = Colors.redAccent;
      txtColor = Colors.white;
      break;
    case GeneralConstant.NONE:
      bgColor = Colors.white;
      txtColor = Colors.black;
      break;
    case GeneralConstant.PENDING:
      bgColor = Colors.yellow;
      txtColor = Colors.black;
      break;
    case GeneralConstant.CONFIRM:
      bgColor = Colors.black54;
      txtColor = Colors.white;
      break;
    case GeneralConstant.LOADING:
      break;
    case GeneralConstant.CONNECTIVITY:
      break;

    default:
      bgColor = Colors.white;
      txtColor = Colors.black;
  }

  Fluttertoast.showToast(
      msg: message!,
      toastLength: duration,
      gravity: ToastGravity.BOTTOM,
      timeInSecForIosWeb: 1,
      backgroundColor: bgColor,
      textColor: txtColor,
      fontSize: 16.0);
}

enum GeneralConstant {
  SUCCESS,
  FAILED,
  NONE,
  PENDING,
  LOADING,
  CONFIRM,
  CONNECTIVITY
}