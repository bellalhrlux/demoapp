import 'dart:convert';

import 'package:demo_app/models/login_response_model.dart';
import 'package:demo_app/utils/cons.dart';
import 'package:demo_app/utils/helpers.dart';
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;

class LoginController with ChangeNotifier {
  late LoginResponseModel responseModel;

  bool _isEmailValid = true;
  bool _isPassValid = true;

  String _emailErrorMsg = '';
  String _passErrorMsg = '';

  bool isLoading=false;

  String get emailErrorMsg => _emailErrorMsg;

  set emailErrorMsg(String value) {
    _emailErrorMsg = value;
  }

  bool get isEmailValid => _isEmailValid;

  set isEmailValid(bool value) {
    _isEmailValid = value;
  }

  bool get isPassValid => _isPassValid;

  set isPassValid(bool value) {
    _isPassValid = value;
  }

  bool formValidator(String email, String password) {
    bool isValid = true;
    if (email.isEmpty) {
      _isEmailValid = false;
      isValid = false;
      _emailErrorMsg = 'Enter your email';
    } else {
      _isEmailValid = true;
      isValid = true;
    }
    if (password.isEmpty) {
      _isPassValid = false;
      isValid = false;
      _passErrorMsg = 'Enter your password';
    } else if (password.length < 6) {
      _passErrorMsg = 'Password at least 6 characters';
      _isPassValid = false;
      isValid = false;
    } else {
      _isPassValid = true;
      isValid = true;
    }

    notifyListeners();

    return isValid;
  }

  String get passErrorMsg => _passErrorMsg;

  set passErrorMsg(String value) {
    _passErrorMsg = value;
  }

  Future<bool> userLogin(String username,String password) async {
    isLoading=true;
    notifyListeners();
    var url = Uri.parse(baseUrl + 'wp-json/jwt-auth/v1/token');
    var response = await http.post(url, body: {
          'username': username,
          'password': password
        });

    //print(response.body);

    isLoading=false;
    notifyListeners();
    if(response.statusCode==200)
      {
        responseModel=LoginResponseModel.fromJson(json.decode(response.body));
        globalLoginResModel=responseModel;
        return true;
      }
    else if(response.statusCode==403)
      {
        responseModel=LoginResponseModel.fromJson(json.decode(response.body));
        toaster(parseHtmlString(responseModel.message!),toastType: GeneralConstant.FAILED);
        return false;
      }
    else{
      toaster(serverError,toastType: GeneralConstant.FAILED);
      return false;
    }

  }
}
