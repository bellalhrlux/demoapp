
class UserProfileModel {
  UserProfileModel({
      int? id, 
      String? username, 
      String? name, 
      String? firstName, 
      String? lastName, 
      String? email, 
      String? url, 
      String? description, 
      String? link, 
      String? locale, 
      String? nickname, 
      String? slug, 
      List<String>? roles, 
      String? registeredDate, 
      Capabilities? capabilities, 
      Extra_capabilities? extraCapabilities, 
      Avatar_urls? avatarUrls, 
      List<dynamic>? meta, 
     }){
    _id = id;
    _username = username;
    _name = name;
    _firstName = firstName;
    _lastName = lastName;
    _email = email;
    _url = url;
    _description = description;
    _link = link;
    _locale = locale;
    _nickname = nickname;
    _slug = slug;
    _roles = roles;
    _registeredDate = registeredDate;
    _capabilities = capabilities;
    _extraCapabilities = extraCapabilities;
    _avatarUrls = avatarUrls;
}

  UserProfileModel.fromJson(dynamic json) {
    _id = json['id'];
    _username = json['username'];
    _name = json['name'];
    _firstName = json['first_name'];
    _lastName = json['last_name'];
    _email = json['email'];
    _url = json['url'];
    _description = json['description'];
    _link = json['link'];
    _locale = json['locale'];
    _nickname = json['nickname'];
    _slug = json['slug'];
    _roles = json['roles'] != null ? json['roles'].cast<String>() : [];
    _registeredDate = json['registered_date'];
    _capabilities = json['capabilities'] != null ? Capabilities.fromJson(json['capabilities']) : null;
    _extraCapabilities = json['extra_capabilities'] != null ? Extra_capabilities.fromJson(json['extra_capabilities']) : null;
    _avatarUrls = json['avatar_urls'] != null ? Avatar_urls.fromJson(json['avatar_urls']) : null;

  }
  int? _id;
  String? _username;
  String? _name;
  String? _firstName;
  String? _lastName;
  String? _email;
  String? _url;
  String? _description;
  String? _link;
  String? _locale;
  String? _nickname;
  String? _slug;
  List<String>? _roles;
  String? _registeredDate;
  Capabilities? _capabilities;
  Extra_capabilities? _extraCapabilities;
  Avatar_urls? _avatarUrls;

  int? get id => _id;
  String? get username => _username;
  String? get name => _name;
  String? get firstName => _firstName;
  String? get lastName => _lastName;
  String? get email => _email;
  String? get url => _url;
  String? get description => _description;
  String? get link => _link;
  String? get locale => _locale;
  String? get nickname => _nickname;
  String? get slug => _slug;
  List<String>? get roles => _roles;
  String? get registeredDate => _registeredDate;
  Capabilities? get capabilities => _capabilities;
  Extra_capabilities? get extraCapabilities => _extraCapabilities;
  Avatar_urls? get avatarUrls => _avatarUrls;


  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['username'] = _username;
    map['name'] = _name;
    map['first_name'] = _firstName;
    map['last_name'] = _lastName;
    map['email'] = _email;
    map['url'] = _url;
    map['description'] = _description;
    map['link'] = _link;
    map['locale'] = _locale;
    map['nickname'] = _nickname;
    map['slug'] = _slug;
    map['roles'] = _roles;
    map['registered_date'] = _registeredDate;
    if (_capabilities != null) {
      map['capabilities'] = _capabilities?.toJson();
    }
    if (_extraCapabilities != null) {
      map['extra_capabilities'] = _extraCapabilities?.toJson();
    }
    if (_avatarUrls != null) {
      map['avatar_urls'] = _avatarUrls?.toJson();
    }

    return map;
  }

}

/// self : [{"href":"https://apptest.dokandemo.com/wp-json/wp/v2/users/117"}]
/// collection : [{"href":"https://apptest.dokandemo.com/wp-json/wp/v2/users"}]

class _links {
  _links({
      List<Self>? self, 
      List<Collection>? collection,}){
    _self = self;
    _collection = collection;
}

  _links.fromJson(dynamic json) {
    if (json['self'] != null) {
      _self = [];
      json['self'].forEach((v) {
        _self?.add(Self.fromJson(v));
      });
    }
    if (json['collection'] != null) {
      _collection = [];
      json['collection'].forEach((v) {
        _collection?.add(Collection.fromJson(v));
      });
    }
  }
  List<Self>? _self;
  List<Collection>? _collection;

  List<Self>? get self => _self;
  List<Collection>? get collection => _collection;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (_self != null) {
      map['self'] = _self?.map((v) => v.toJson()).toList();
    }
    if (_collection != null) {
      map['collection'] = _collection?.map((v) => v.toJson()).toList();
    }
    return map;
  }

}

/// href : "https://apptest.dokandemo.com/wp-json/wp/v2/users"

class Collection {
  Collection({
      String? href,}){
    _href = href;
}

  Collection.fromJson(dynamic json) {
    _href = json['href'];
  }
  String? _href;

  String? get href => _href;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['href'] = _href;
    return map;
  }

}

/// href : "https://apptest.dokandemo.com/wp-json/wp/v2/users/117"

class Self {
  Self({
      String? href,}){
    _href = href;
}

  Self.fromJson(dynamic json) {
    _href = json['href'];
  }
  String? _href;

  String? get href => _href;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['href'] = _href;
    return map;
  }

}

/// 24 : "https://secure.gravatar.com/avatar/9b1ff02436846f93bd299378c3dfddf5?s=24&d=mm&r=g"
/// 48 : "https://secure.gravatar.com/avatar/9b1ff02436846f93bd299378c3dfddf5?s=48&d=mm&r=g"
/// 96 : "https://secure.gravatar.com/avatar/9b1ff02436846f93bd299378c3dfddf5?s=96&d=mm&r=g"

class Avatar_urls {
  Avatar_urls({
      String? image_24,
      String? image_48,
      String? image_96,}){
    _24 = image_24;
    _48 = image_48;
    _96 = image_96;
}

  Avatar_urls.fromJson(dynamic json) {
    _24 = json['24'];
    _48 = json['48'];
    _96 = json['96'];
  }
  String? _24;
  String? _48;
  String? _96;

  String? get image_24 => _24;
  String? get image_48 => _48;
  String? get image_96 => _96;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['24'] = _24;
    map['48'] = _48;
    map['96'] = _96;
    return map;
  }

}

/// subscriber : true

class Extra_capabilities {
  Extra_capabilities({
      bool? subscriber,}){
    _subscriber = subscriber;
}

  Extra_capabilities.fromJson(dynamic json) {
    _subscriber = json['subscriber'];
  }
  bool? _subscriber;

  bool? get subscriber => _subscriber;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['subscriber'] = _subscriber;
    return map;
  }

}

/// read : true
/// level_0 : true
/// subscriber : true

class Capabilities {
  Capabilities({
      bool? read, 
      bool? level0, 
      bool? subscriber,}){
    _read = read;
    _level0 = level0;
    _subscriber = subscriber;
}

  Capabilities.fromJson(dynamic json) {
    _read = json['read'];
    _level0 = json['level_0'];
    _subscriber = json['subscriber'];
  }
  bool? _read;
  bool? _level0;
  bool? _subscriber;

  bool? get read => _read;
  bool? get level0 => _level0;
  bool? get subscriber => _subscriber;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['read'] = _read;
    map['level_0'] = _level0;
    map['subscriber'] = _subscriber;
    return map;
  }

}