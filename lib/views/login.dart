import 'package:demo_app/constrollers/login_controller.dart';
import 'package:demo_app/utils/app_colors.dart';
import 'package:demo_app/utils/common_widgets.dart';
import 'package:demo_app/utils/cons.dart';
import 'package:demo_app/utils/dimens.dart';
import 'package:demo_app/utils/routes.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';


class Login extends StatefulWidget {
  const Login({Key? key}) : super(key: key);

  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {

  /// declare home controller
  late LoginController _loginController;
  ///Declare controller for textField
  final _emailController= TextEditingController();
  final _passwordController= TextEditingController();

  Text _loginTitle() => const Text(
        loginNow,
        style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold),
      );

  Padding _loginSubtitle() => const Padding(
        padding: EdgeInsets.only(top: 15,bottom: 15),
        child: Text(
          loginDetailMsg,
          style: TextStyle(fontSize: 12, fontWeight: FontWeight.w300),
        ),
      );

  Padding _loginForm() => Padding(child: Column(
    children: [
      textFieldContainer('Enter email',controller: _emailController),
      if(!_loginController.isEmailValid)
      textFieldErrorMsg(msg: _loginController.emailErrorMsg),
      const SizedBox(height: 20,),
      textFieldContainer('Enter Password',obscureValue: true,controller: _passwordController),
      if(!_loginController.isPassValid)
      textFieldErrorMsg(msg: _loginController.passErrorMsg)
    ],
  ),padding: const EdgeInsets.only(left: 20,right: 20),);

  Padding _createNewAccount() =>Padding(padding: const EdgeInsets.only(left: 20,right: 20,top: 30),child: Row(
    mainAxisAlignment: MainAxisAlignment.center,
    crossAxisAlignment: CrossAxisAlignment.center,
    children:  [
      const Text('Don\'t have an account?'),
      InkWell(child: const Text('Register',style: TextStyle(fontWeight: FontWeight.bold),),onTap: (){
        Navigator.pushReplacementNamed(context, registerRoute);
      },),

    ],
  ),);

  Padding _loginBtn()=>Padding(padding: const EdgeInsets.only(left: 20,right: 20),child: SizedBox(
    height: buttonHeight,
    width: double.infinity,
    child: ElevatedButton(
        style: ElevatedButton.styleFrom(shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(8)
        )),
        onPressed: ()async{
            var isValid=_loginController.formValidator(_emailController.text,_passwordController.text);
            if(isValid)
              {
                if(!_loginController.isLoading) {
                  var result = await _loginController.userLogin(
                      _emailController.text, _passwordController.text);
                  if (result) {
                    Navigator.pushReplacementNamed(context, homeRoute);
                  }
                }
                //Navigator.pushNamedAndRemoveUntil(context, registerRoute, (route) => false);
              }
        }, child: _loginController.isLoading?const CircularProgressIndicator(color: Colors.white,):const Text('Login',style: TextStyle(fontSize: 16,fontWeight: FontWeight.bold),)),
  ),);


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: null,
      backgroundColor: Colors.white,
      body: Center(
        child: SingleChildScrollView(
          child: ChangeNotifierProvider(
            create: (context) => LoginController(),
            child: Consumer<LoginController>(
              builder: (context,controller,child){
                _loginController=controller;
                return Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    _loginTitle(),
                    _loginSubtitle(),
                    _loginForm(),
                     const SizedBox(height: 40,),
                    _loginBtn(),
                    _createNewAccount()
                  ],
                );
              },
            ),
          ),
        ),
      ),
    );
  }
}
