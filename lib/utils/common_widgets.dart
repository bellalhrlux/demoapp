import 'package:demo_app/utils/app_colors.dart';
import 'package:flutter/material.dart';

Widget textFieldContainer(String level,
    {bool obscureValue = false,
      double blockHeight = 50,
      double hintSize = 14,
      double fontSize=14,
      double boxRadius=8,
      String initValue='',
      bool isEnable=true,
      Color containerColor = lightWhite,
      TextInputType inputType = TextInputType.text,
      TextEditingController? controller}) {
  return Container(
    height: blockHeight,
    padding: const EdgeInsets.only(left: 12, right: 10),
    decoration: BoxDecoration(
        color: containerColor, borderRadius: BorderRadius.circular(boxRadius)),
    child: TextFormField(
      obscureText: obscureValue,
      controller: controller,
      enabled: isEnable,
      //initialValue: initValue,
      keyboardType: inputType,
      style: TextStyle(fontSize: fontSize),
      decoration: InputDecoration(

          errorStyle: const TextStyle(color: Colors.redAccent ),
          border: InputBorder.none,
          hintStyle: TextStyle(fontSize: hintSize),
          hintText: level),
      onChanged: (String value) {},
    ),
  );
}

Widget textFieldErrorMsg({String msg='None',Alignment alignment=Alignment.centerRight,EdgeInsetsGeometry paddingVal=const EdgeInsets.only(left: 0.0,right: 2.0,top: 3.0,bottom: 0.0)}){
  return Align(alignment:alignment,child: Padding(child: Text(msg,style:const  TextStyle(color: Colors.redAccent,fontSize: 11),textAlign: TextAlign.right,),padding: paddingVal,));
}